﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    public float speed = 5.0f;

    private Vector3 input; //Tracks input as numbers
    private Vector3 motion; //Tracks movement step per frame
    private CharacterController controller;

    //Awake is called before Start
    private void Awake()
    {
        controller = GetComponent<CharacterController>();
    }


    // Update is called once per frame
    void Update()
    {
        motion = Vector3.zero;
        input = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Depth"), Input.GetAxis("Vertical"));
        motion += transform.forward.normalized * input.z;
        motion += transform.right.normalized * input.x;
        motion += Vector3.up.normalized * input.y;
        controller.Move(motion * speed * Time.deltaTime);
    }

    private void Move(Vector3 vector3)
    {
        throw new NotImplementedException();
    }
}
